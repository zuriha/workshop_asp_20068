namespace SUREEHA20068.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Drawing;
    using SUREEHA20068.Models;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class AppDb : IdentityDbContext<ApplicationUser>
    {
        public static AppDb Create()
        {
            return new AppDb();

        }

        public AppDb(): base("AppDb")
        {
            
        }

        public DbSet<Product> products { get; set; }
        public DbSet<Categories> categories { get; set; }

        public DbSet<Supplier> suppliers { get; set; }

        public DbSet<Order> orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

        public DbSet<Employee> employees { get; set; }

        public DbSet<Customer> Customers { get; set; }


        public DbSet<Shipper> shippers { get; set; }

     
        public DbSet<test> tests { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
