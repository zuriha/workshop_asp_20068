﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SUREEHA20068.Models
{
    [Table("test")]
    public class test
    {


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID Test")]
        [Index]
        [Column("test_id")]
        public int TestId { get; set; }




        [Required]
        [Display(Name = "ชื่อคุณ")]
        [StringLength(50, ErrorMessage = "product name must be at least 2 charector", MinimumLength = 2)]
        [Column("test_name")]
        public string TestName { get; set; }




    }
}