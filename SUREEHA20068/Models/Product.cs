﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SUREEHA20068.Models
{
    [Table("Products")]
    public class Product
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID Product")]
        [Index]
        [Column("product_id")]
        public int ProductId { get; set; }


        [Required]
        [Display(Name = "ชื่อสินค้า")]
        [StringLength(50, ErrorMessage = "product name must be at least 2 charector", MinimumLength = 2)]
        [Column("product_name")]
        public string ProductName { get; set; }


        [Required]
        [Display(Name = "ปริมาณ/จำนวน")]
        [StringLength(50, ErrorMessage = "quantity_per_unit must be at least 2 charector", MinimumLength = 2)]
        [Column("quantity_per_unit")]
        public string QuantityPerUnit { get; set; }


        [Display(Name = "รายละเอียดเพิ่มเติม")]
        [StringLength(50, ErrorMessage = "Detail must be at least 2 charector", MinimumLength = 2)]
        [Column("detail")]
        public string Detail { get; set; }



        [Required]
        [Display(Name = "รูปภาพ")]
        [StringLength(200, ErrorMessage = "Detail must be at least 2 charector", MinimumLength = 2)]
        [Column("imagepath")]
        public string ImagePath { get; set; }




        [Required]
        [Display(Name = "ราคาต่อหน่วย")]
        [Column("unit_price")]
        public decimal UnitPrice { get; set; }


        [Display(Name = "สร้างโดย")]
        [StringLength(100, ErrorMessage = "createby must be at least 2 charector", MinimumLength = 2)]
        [Column("create_by")]
        public string CreateBy { get; set; }



        [Display(Name = "วันที่สร้าง")]
        [Column("create_date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; } = DateTime.Now;





        //Relational(foreign key)//category_id
        [Column("category_id")]
        public int CategoryId { get; set; } //เปลี่ยน
        public Categories categories { get; set; }



        //Relational(foreign key)//SupplierId
        [Column("supplier_id")]
        public int SupplierId { get; set; }
        public Supplier supplier { get; set; }







    }
}