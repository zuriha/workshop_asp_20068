﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SUREEHA20068.Models
{

    [Table("orders")]
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID Order")]
        [Index]
        [Column("order_id")]
        public int orderId { get; set; }




        [Required]
        [Display(Name = "วันสั่งสินค้า")]
        [Column("order_date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime OrderDate { get; set; } = DateTime.Now;


     
        [Required]
        [Display(Name = "ค่าจัดส่ง")]
        [Column("freight")]
        public int Freight { get; set; }




        //Relational(foreign key)//
        [Column("customer_id")]
        public int CustomerId { get; set; }
        public Customer customer { get; set; }


        [Column("employee_id")]
        public int EmployeeId { get; set; }
        public Employee employee { get; set; }



        //Relational(foreign key)//
        [Column("shipper_via")]
        public int ShipperId { get; set; }
        public Shipper shipper { get; set; }



    }
}