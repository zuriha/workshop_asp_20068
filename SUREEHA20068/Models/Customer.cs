﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SUREEHA20068.Models
{

    [Table("customers")]
    public class Customer
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID Customer")]
        [Index]
        [Column("customer_id")]
        public int CustomerId { get; set; }


        [Required]
        [Display(Name = "ชื่อ-สกุล")]
        [StringLength(100, ErrorMessage = "product name must be at least 2 charector", MinimumLength = 2)]
        [Column("customer_name")]
        public string Contacename { get; set; }


        [Required]
        [Display(Name = "ที่อยู่")]
        [StringLength(100, ErrorMessage = "address must be at least 2 charector", MinimumLength = 2)]
        [Column("address")]
        public string Address { get; set; }



        [Required]
        [Display(Name = "เบอร์ติดต่อ")]
        [StringLength(100, ErrorMessage = "phone must be at least 2 charector", MinimumLength = 2)]
        [Column("phone")]
        public string Phone { get; set; }












    }
}