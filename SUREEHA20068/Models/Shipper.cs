﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SUREEHA20068.Models
{

    [Table("shippers")]
    public class Shipper
    {


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID Shipper")]
        [Index]
        [Column("shipper_id")]
        public int ShipperId { get; set; }




        [Required]
        [Display(Name = "ชื่อบริษัท")]
        [StringLength(50, ErrorMessage = "product name must be at least 2 charector", MinimumLength = 2)]
        [Column("company_name")]
        public string CompanyName { get; set; }


        [Required]
        [Display(Name = "เบอร์ติดต่อ")]
        [StringLength(50, ErrorMessage = "product name must be at least 2 charector", MinimumLength = 2)]
        [Column("phone")]
        public string Phone { get; set; }


        [Display(Name = "สร้างโดย")]
        [StringLength(100, ErrorMessage = "createby must be at least 2 charector", MinimumLength = 2)]
        [Column("create_by")]
        public string CreateBy { get; set; }



        [Display(Name = "วันที่สร้าง")]
        [Column("create_date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; } = DateTime.Now;








    }
}