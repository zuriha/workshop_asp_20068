﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SUREEHA20068.Models
{

    [Table("orderdetail")]
    public class OrderDetail
    {

        //กรณีที่ตารางไม่มี key เป็นของตัวเอง //order_id
        [Key, Column("order_id", Order = 0)]
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }

        //กรณีที่ตารางไม่มี key เป็นของตัวเอง //product_id
        [Key, Column("product_id", Order = 1)]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }



        [Required] //เพื่อกำหนดให้ค่าต้องไม่ว่าง
        [Display(Name = "ราคาต่อหน่วย")]//ค่าหน้า input 

        [Column("unit_price")]
        public decimal UnitPrice { get; set; }


        [Required]
        [Display(Name = "จำนวน")]

        [Column("quantity")]
        [Range(1, int.MaxValue, ErrorMessage = "The Quantity must be value greater than 0")]
        public int Quantity { get; set; }














    }
}