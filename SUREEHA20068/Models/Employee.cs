﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SUREEHA20068.Models
{

    [Table("employees")]
    public class Employee
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Id Employee")]
        [Index]
        [Column("employee_id")]
        public int EmployeeId { get; set; }



        [Required]
        [Display(Name = "ชื่อ-สกุล")]
        [StringLength(100, ErrorMessage = "product name must be at least 2 charector", MinimumLength = 2)]
        [Column("employee_name")]
        public string FirstName { get; set; }




        [Required]
        [Display(Name = "วันเ/ดือน/ปี เกิด")]

        [Column("birth_date")]
        public DateTime BirthDate { get; set; } //ตรวจ



        [Required]
        [Display(Name = "วันเริ่มงาน")]
        [Column("hire_date")]
        public DateTime HireDate { get; set; } //ตรวจ


        [Required]
        [Display(Name = "ที่อยู่")]
        [StringLength(100, ErrorMessage = "product name must be at least 2 charector", MinimumLength = 2)]
        [Column("adress")]
        public string Adress { get; set; }





    }
}