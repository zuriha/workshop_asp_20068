﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SUREEHA20068.Models
{
    [Table("categories")]
    public class Categories
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID Category")]
        [Index]
        [Column("category_id")]
        public int CategoryId { get; set; }


        [Required]
        [Display(Name = "ชื่อประเภทสินค้า")]
        [StringLength(100, ErrorMessage = "product name must be at least 2 charector", MinimumLength = 2)]
        [Column("category_name")]
        public string CategoryName { get; set; }



        [Display(Name = "ลักษณะสินค้า")]
        [StringLength(100, ErrorMessage = "product name must be at least 2 charector", MinimumLength = 2)]
        [Column("description")]
        public string Description { get; set; }



        [Display(Name = "สร้างโดย")]
        [StringLength(100, ErrorMessage = "createby must be at least 2 charector", MinimumLength = 2)]
        [Column("create_by")]
        public string CreateBy { get; set; }


        [Required]
        [Display(Name = "วันที่สร้าง")]
        [Column("create_date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreateDate { get; set; } = DateTime.Now;


    }
}