﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SUREEHA20068.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


namespace SUREEHA20068.Controllers
{
    public class ImageController : Controller
    {
        [HttpGet]
        // GET: Image
        public ActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(SUREEHA20068.Models.Image imageModel)
        {
            string fileName = Path.GetFileNameWithoutExtension(imageModel.ImageFile.FileName);
            string extenstion = Path.GetExtension(imageModel.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extenstion;
            imageModel.ImagePath = "~/Image/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
            imageModel.ImageFile.SaveAs(fileName);
            //using 
                return View();
            
        }
    }
}