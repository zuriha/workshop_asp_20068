﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SUREEHA20068.Data;
using SUREEHA20068.Models;

namespace SUREEHA20068.Controllers
{
    public class OrdersController : Controller
    {
        private AppDb db = new AppDb();

        // GET: Orders
        public async Task<ActionResult> Index()
        {
            var orders = db.orders.Include(o => o.customer).Include(o => o.employee).Include(o => o.shipper);
            return View(await orders.ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "Contacename");
            ViewBag.EmployeeId = new SelectList(db.employees, "EmployeeId", "FirstName");
            ViewBag.ShipperId = new SelectList(db.shippers, "ShipperId", "CompanyName");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "orderId,OrderDate,Freight,CustomerId,EmployeeId,ShipperId")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.orders.Add(order);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "Contacename", order.CustomerId);
            ViewBag.EmployeeId = new SelectList(db.employees, "EmployeeId", "FirstName", order.EmployeeId);
            ViewBag.ShipperId = new SelectList(db.shippers, "ShipperId", "CompanyName", order.ShipperId);
            return View(order);
        }

        // GET: Orders/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "Contacename", order.CustomerId);
            ViewBag.EmployeeId = new SelectList(db.employees, "EmployeeId", "FirstName", order.EmployeeId);
            ViewBag.ShipperId = new SelectList(db.shippers, "ShipperId", "CompanyName", order.ShipperId);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "orderId,OrderDate,Freight,CustomerId,EmployeeId,ShipperId")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "Contacename", order.CustomerId);
            ViewBag.EmployeeId = new SelectList(db.employees, "EmployeeId", "FirstName", order.EmployeeId);
            ViewBag.ShipperId = new SelectList(db.shippers, "ShipperId", "CompanyName", order.ShipperId);
            return View(order);
        }

        // GET: Orders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = await db.orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Order order = await db.orders.FindAsync(id);
            db.orders.Remove(order);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
