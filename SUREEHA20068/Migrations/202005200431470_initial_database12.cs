namespace SUREEHA20068.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database12 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Products", name: "supplier Id", newName: "supplier_id");
            RenameIndex(table: "dbo.Products", name: "IX_supplier Id", newName: "IX_supplier_id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Products", name: "IX_supplier_id", newName: "IX_supplier Id");
            RenameColumn(table: "dbo.Products", name: "supplier_id", newName: "supplier Id");
        }
    }
}
