namespace SUREEHA20068.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database06 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "imagepath", c => c.String(nullable: false, maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "imagepath");
        }
    }
}
