namespace SUREEHA20068.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database01 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.categories",
                c => new
                    {
                        category_id = c.Int(nullable: false, identity: true),
                        category_name = c.String(nullable: false, maxLength: 100),
                        description = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.category_id)
                .Index(t => t.category_id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        product_id = c.Int(nullable: false, identity: true),
                        product_name = c.String(nullable: false, maxLength: 50),
                        quantity_per_unit = c.String(nullable: false, maxLength: 50),
                        unit_price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        category_id = c.Int(nullable: false),
                        supplierId = c.Int(name: "supplier Id", nullable: false),
                    })
                .PrimaryKey(t => t.product_id)
                .ForeignKey("dbo.categories", t => t.category_id, cascadeDelete: true)
                .ForeignKey("dbo.suppliers", t => t.supplierId, cascadeDelete: true)
                .Index(t => t.product_id)
                .Index(t => t.category_id)
                .Index(t => t.supplierId);
            
            CreateTable(
                "dbo.suppliers",
                c => new
                    {
                        supplier_id = c.Int(nullable: false, identity: true),
                        company_name = c.String(nullable: false, maxLength: 100),
                        contact_name = c.String(nullable: false, maxLength: 100),
                        address = c.String(nullable: false, maxLength: 100),
                        phone = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.supplier_id)
                .Index(t => t.supplier_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "supplier Id", "dbo.suppliers");
            DropForeignKey("dbo.Products", "category_id", "dbo.categories");
            DropIndex("dbo.suppliers", new[] { "supplier_id" });
            DropIndex("dbo.Products", new[] { "supplier Id" });
            DropIndex("dbo.Products", new[] { "category_id" });
            DropIndex("dbo.Products", new[] { "product_id" });
            DropIndex("dbo.categories", new[] { "category_id" });
            DropTable("dbo.suppliers");
            DropTable("dbo.Products");
            DropTable("dbo.categories");
        }
    }
}
