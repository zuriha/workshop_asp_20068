namespace SUREEHA20068.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database08 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.categories", "createby", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.categories", "createby");
        }
    }
}
