namespace SUREEHA20068.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database05 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.employees",
                c => new
                    {
                        employee_id = c.Int(nullable: false, identity: true),
                        employee_name = c.String(nullable: false, maxLength: 100),
                        birth_date = c.DateTime(nullable: false),
                        hire_date = c.DateTime(nullable: false),
                        adress = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.employee_id)
                .Index(t => t.employee_id);
            
            CreateTable(
                "dbo.orderdetail",
                c => new
                    {
                        order_id = c.Int(nullable: false),
                        product_id = c.Int(nullable: false),
                        unit_price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.order_id, t.product_id })
                .ForeignKey("dbo.orders", t => t.order_id, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.product_id, cascadeDelete: true)
                .Index(t => t.order_id)
                .Index(t => t.product_id);
            
            CreateTable(
                "dbo.orders",
                c => new
                    {
                        order_id = c.Int(nullable: false, identity: true),
                        order_date = c.DateTime(nullable: false),
                        freight = c.Int(nullable: false),
                        customer_id = c.Int(nullable: false),
                        employee_id = c.Int(nullable: false),
                        shipper_via = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.order_id)
                .ForeignKey("dbo.customers", t => t.customer_id, cascadeDelete: true)
                .ForeignKey("dbo.employees", t => t.employee_id, cascadeDelete: true)
                .ForeignKey("dbo.shippers", t => t.shipper_via, cascadeDelete: true)
                .Index(t => t.order_id)
                .Index(t => t.customer_id)
                .Index(t => t.employee_id)
                .Index(t => t.shipper_via);
            
            CreateTable(
                "dbo.shippers",
                c => new
                    {
                        shipper_id = c.Int(nullable: false, identity: true),
                        company_name = c.String(nullable: false, maxLength: 50),
                        phone = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.shipper_id)
                .Index(t => t.shipper_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.orderdetail", "product_id", "dbo.Products");
            DropForeignKey("dbo.orderdetail", "order_id", "dbo.orders");
            DropForeignKey("dbo.orders", "shipper_via", "dbo.shippers");
            DropForeignKey("dbo.orders", "employee_id", "dbo.employees");
            DropForeignKey("dbo.orders", "customer_id", "dbo.customers");
            DropIndex("dbo.shippers", new[] { "shipper_id" });
            DropIndex("dbo.orders", new[] { "shipper_via" });
            DropIndex("dbo.orders", new[] { "employee_id" });
            DropIndex("dbo.orders", new[] { "customer_id" });
            DropIndex("dbo.orders", new[] { "order_id" });
            DropIndex("dbo.orderdetail", new[] { "product_id" });
            DropIndex("dbo.orderdetail", new[] { "order_id" });
            DropIndex("dbo.employees", new[] { "employee_id" });
            DropTable("dbo.shippers");
            DropTable("dbo.orders");
            DropTable("dbo.orderdetail");
            DropTable("dbo.employees");
        }
    }
}
