namespace SUREEHA20068.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database121 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.suppliers", "create_by", c => c.String(maxLength: 100));
            AddColumn("dbo.suppliers", "create_date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.suppliers", "create_date");
            DropColumn("dbo.suppliers", "create_by");
        }
    }
}
