namespace SUREEHA20068.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database04 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.customers",
                c => new
                    {
                        customer_id = c.Int(nullable: false, identity: true),
                        customer_name = c.String(nullable: false, maxLength: 100),
                        address = c.String(nullable: false, maxLength: 100),
                        phone = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.customer_id)
                .Index(t => t.customer_id);
            
            AddColumn("dbo.Products", "detail", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropIndex("dbo.customers", new[] { "customer_id" });
            DropColumn("dbo.Products", "detail");
            DropTable("dbo.customers");
        }
    }
}
