namespace SUREEHA20068.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.shippers", "create_by", c => c.String(maxLength: 100));
            AddColumn("dbo.shippers", "create_date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.shippers", "create_date");
            DropColumn("dbo.shippers", "create_by");
        }
    }
}
