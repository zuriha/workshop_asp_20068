namespace SUREEHA20068.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database09 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.categories", name: "createby", newName: "create_by");
            AddColumn("dbo.categories", "create_date", c => c.DateTime(nullable: false));
            AddColumn("dbo.Products", "create_by", c => c.String(maxLength: 100));
            AddColumn("dbo.Products", "create_date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "create_date");
            DropColumn("dbo.Products", "create_by");
            DropColumn("dbo.categories", "create_date");
            RenameColumn(table: "dbo.categories", name: "create_by", newName: "createby");
        }
    }
}
