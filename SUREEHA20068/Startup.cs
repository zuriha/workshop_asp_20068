﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SUREEHA20068.Startup))]
namespace SUREEHA20068
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
